package Step_definations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;

import com.leadiq.utills.extPage;
import com.leadiq.utills.signUpPage;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;


public class KeywordImpl {

	public static WebDriver driver;
	
	
	private final String driverLocation = System.getProperty("user.dir")+"/src/main/resources/chromedriver";
	private final String extFile = "/src/main/resources/LeadIQExt.crx";
	private final String mylinkdinPrf = "https://www.linkedin.com/in/jeedimetla";
	private final String leadIQAlinkdinSearch = "https://www.linkedin.com/search/results/all/?keywords=LeadIQ&origin=GLOBAL_SEARCH_HEADER";
	private final String leadIQUser = "test"+new SimpleDateFormat("MMddhhmmss").format(new Date());
	extPage objextPage = new extPage(driver);
	
	@When("^open Browser with added leadIQ crx$")
	public void open_Browser_with_added_leadIQ_crx()  throws Throwable
	{
		try {
		System.setProperty("webdriver.chrome.driver", driverLocation);
		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File(System.getProperty("user.dir")+extFile));
		options.addArguments("–load-extension="+System.getProperty("user.dir")+extFile);
		driver = new ChromeDriver(options);
		}
		catch(Exception e)
		{
			Assert.fail("Failed open browser with extension installation due to reason- "+e.toString());
		}
		Assert.assertTrue(true,"Extension installed successfully");
		return ;
	}
	
	@When("^Signup with new user$")
	public void Signup_with_new_user() throws Throwable {
		
		new signUpPage(driver).createLogin(leadIQUser);
	}

	@When("^Default campigen name should be user's campigen$")
	public void verify_default_campigan() throws InterruptedException {
		
		objextPage.openLeadIQExt();
		objextPage.verifyDefaultCampaign(leadIQUser+"'s Campaign");
	}
	
	@Given("^open new tab$")
	public void openNewTab() throws AWTException {
		Robot r = new Robot();        
        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_T);
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyRelease(KeyEvent.VK_T);
	}
	
	@When("^login to linkdin and capture$")
	public void login_to_linkdin_and_capture() throws InterruptedException {
		
		objextPage.captureLead(mylinkdinPrf);
		objextPage.captureLead(leadIQAlinkdinSearch);
		objextPage.captureLead(leadIQAlinkdinSearch+"&page=2");
		
	}
	
	@When("^details of leads should be avalible$")
	public void verify_capture_lead() {
		objextPage.verifyCapturedLeads();
	}
	
	@When("^Close Browser$")
	public void close_Browser() {
		driver.quit();
	}

}
