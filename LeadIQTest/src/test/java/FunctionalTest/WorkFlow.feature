
@tag
Feature: Lead capture workflow

	@tag1
  Scenario: Install LeadIQ extension
    When open Browser with added leadIQ crx
    
  @tag2
  Scenario: Create LeadIQA account
    When Signup with new user

	@tag3
	Scenario: Verify Default Campigen
    When Default campigen name should be user's campigen
   
  @tag4
  Scenario: login to linkdin
  Given open new tab
  Then login to linkdin and capture
  
  @tag5
  Scenario: Verify captured leads
  When details of leads should be avalible
  
  @tag5
  Scenario: Close Browser
  When Close Browser
