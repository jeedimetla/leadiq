package com.leadiq.utills;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;

public class signUpPage {
	
	WebDriver driver;
	
	private final String signupURL = "https://account.leadiq.com/app/signup/?referralCode=goneprospectin";
	
	int timeout = 20;
	
	By firstNametxtBox = By.xpath("//input[@type='text' and @placeholder='First Name']");
	
	By lastNametxtBox = By.xpath("//input[@type='text' and @placeholder='Last Name']");
	
	By emailtxtBox = By.xpath("//input[@type='text' and @placeholder='Email']");
	
	By companyNametxtBox = By.xpath("//input[@type='text' and @placeholder='Company Name']");
	
	By passwordBox = By.xpath("//input[@type='password' and @placeholder='Password']");
	
	By confirmPasswordBox = By.xpath("//input[@type='password' and @placeholder='Confirm Password']");
	
	By submitBtn = By.xpath("//div[@id='app']//div[contains(@class, 'iq-button')]");
	
	By signoutBtn = By.xpath("//div[contains(text(),'Sign out')]");
	
	public signUpPage(WebDriver driver) {
		this.driver = driver;
		
	}

	@Test
	public void createLogin(String user) {
		
		driver.get(signupURL);
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		driver.findElement(firstNametxtBox).clear();
		driver.findElement(firstNametxtBox).sendKeys(user);
		driver.findElement(lastNametxtBox).clear();
		driver.findElement(lastNametxtBox).sendKeys(user);
		driver.findElement(emailtxtBox).clear();
		driver.findElement(emailtxtBox).sendKeys(user+"@gmail.com");
		driver.findElement(companyNametxtBox).clear();
		driver.findElement(companyNametxtBox).sendKeys(user);
		driver.findElement(passwordBox).clear();
		driver.findElement(confirmPasswordBox).clear();
		driver.findElement(passwordBox).sendKeys(user);
		driver.findElement(confirmPasswordBox).sendKeys(user);
		driver.findElement(submitBtn).click();
		Assert.assertNotNull(this.driver.findElements(signoutBtn), "Sign up is not successful");
		
	}
}
