package com.leadiq.utills;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class linkdinPage {
	
	WebDriver driver;
	private final String loginLink = "https://www.linkedin.com/login";
	private final String testUsr = "dineshjdmt@gmail.com";
	private final String testUsrPwd = "JDLeadIQ1";
	
	By emailtxtBox = By.xpath("//*[@id='username']");
	By passwordtxtBox = By.xpath("//*[@id='password']");
	By submitBtn = By.xpath("//button[@type='submit']");
	
	public linkdinPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void openProfile(String profileURL) throws InterruptedException {
		
		driver.get(profileURL);
		
	}
	
	public void loginwithLinkdinUsr() throws InterruptedException {
		
		driver.get(loginLink);
		driver.findElement(emailtxtBox).clear();
		driver.findElement(emailtxtBox).sendKeys(testUsr);
		driver.findElement(passwordtxtBox).clear();
		driver.findElement(passwordtxtBox).sendKeys(testUsrPwd);
		driver.findElement(submitBtn).click();
	}
	

}
