package com.leadiq.utills;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
 features = "src/test/java/FunctionalTest",
 glue= "Step_definations"
 )

public class TestRunner {

}
