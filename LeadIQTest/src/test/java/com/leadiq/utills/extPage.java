package com.leadiq.utills;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import java.util.ArrayList;

public class extPage {

	WebDriver driver;
	
	private final String extURL = "https://account.leadiq.com/extension/leadiq-app.html#/init";
	private final String webAppCampiang = "https://account.leadiq.com/app/campaigns/";
	
	By campaignSpan = By.xpath("//ul[@class='el-select-group']/.//span");
	By captureBtn = By.xpath("//div[contains(@class, 'iq-button')]/span");
	By campaignDwnArrow = By.xpath("//span[@class='el-input__suffix']//i");
	By webAppLink = By.xpath("//img[@class='view-button']");
	By campaigenList = By.xpath("//div[contains(@class, 'campaign-name') and contains(text(), '\'s Campaign')]");
	By phoneCheckboxes = By.xpath("//input[@type='checkbox']");
	By leadRecords = By.xpath("//div[@ref='eBodyViewport']/div[1]/div[@role='row']");
	By emailRecords = By.xpath("//div[@ref='eBodyViewport']//div[@col-id='workEmail']");
	By nameRecords = By.xpath("//div[@ref='eBodyViewport']//div[@col-id='name']");
	By phoneRecords = By.xpath("//div[@ref='eBodyViewport']//div[@col-id='workPhones']");
	
	public extPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void openLeadIQExt() throws InterruptedException {
		driver.get(extURL);
		Thread.sleep(5000);
	}
	
	public void verifyDefaultCampaign(String expectedname) {
		
		driver.findElement(campaignDwnArrow).click();
		Assert.assertEquals(driver.findElement(campaignSpan).getText().trim(), expectedname);
	}
	
	public void openWebApp() {
		driver.findElement(webAppLink).click();
	}
	
	public void scrolldown() {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)");
	}
	 
	public void captureLead(String profileUrl) throws InterruptedException {
		
		openWebApp();
		ArrayList<String> tabIds = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabIds.get(1));
		linkdinPage objlinkdinPage = new linkdinPage(driver);
		objlinkdinPage.loginwithLinkdinUsr();
		objlinkdinPage.openProfile(profileUrl);
		scrolldown();
		driver.switchTo().window(tabIds.get(0));
		Thread.sleep(3000);
		driver.switchTo().window(tabIds.get(1));
		Thread.sleep(1000);
		driver.switchTo().window(tabIds.get(0));
		Thread.sleep(5000);
		(new WebDriverWait(driver, 20))
		  .until(ExpectedConditions.presenceOfElementLocated(captureBtn));
		(new WebDriverWait(driver, 20))
		  .until(ExpectedConditions.presenceOfElementLocated(captureBtn)).click();
		
	}
	
	public void verifyCapturedLeads() {
		
		driver.get(webAppCampiang);
		(new WebDriverWait(driver, 20))
		  .until(ExpectedConditions.presenceOfElementLocated(campaigenList)).click();
		
		Assert.assertTrue(driver.findElements(leadRecords).size() > 0, "No Leads captured with email and name");
		Assert.assertTrue(driver.findElements(leadRecords).size() > 4, "Atleast 5 leads are not captured with email and name");
		Assert.assertTrue(driver.findElements(emailRecords).size() > 0, "Leads are not captured with email details");
		Assert.assertTrue(driver.findElements(phoneRecords).size() > 0, "Leads are not captured with phone details");
		Assert.assertTrue(driver.findElements(nameRecords).size() > 0, "Leads names are not captured properly");
	}
	
}
